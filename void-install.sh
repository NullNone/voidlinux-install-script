#!/bin/sh

EFI_PARTITION=/dev/sda1
LVM_PARTITION=/dev/sda2
GRUB_PACKAGE=grub-x86_64-efi
ROOT_SIZE=32G
SWAP_SIZE=8G
NEW_HOSTMANE=host

cryptsetup luksFormat --type luks1 $LVM_PARTITION
cryptsetup luksOpen $LVM_PARTITION voidvm
vgcreate voidvm /dev/mapper/voidvm
lvcreate --name root -L $ROOT_SIZE voidvm
lvcreate --name swap -L $SWAP_SIZE voidvm
lvcreate --name home -l 100%FREE voidvm

mkfs.ext4 -L root /dev/voidvm/root
mkfs.ext4 -L home /dev/voidvm/home
mkswap /dev/voidvm/swap

mount /dev/voidvm/root /mnt
for dir in dev proc sys run; do mkdir -p /mnt/$dir ; mount --rbind /$dir /mnt/$dir ; mount --make-rslave /mnt/$dir ; done
mkdir -p /mnt/home
mount /dev/voidvm/home /mnt/home

mkfs.vfat $EFI_PARTITION
mkdir -p /mnt/boot/efi
mount $EFI_PARTITION /mnt/boot/efi

mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/


xbps-install -Sy -R https://repo-default.voidlinux.org/current -r /mnt base-system lvm2 cryptsetup $GRUB_PACKAGE
chroot /mnt
chown root:root /
chmod 755 /
passwd root
echo $NEW_HOSTMANE > /etc/hostname
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

echo "tmpfs             /tmp        tmpfs   defaults,nosuid,nodev 0       0" >> /etc/fstab
echo "/dev/voidvm/root  /           ext4    defaults,noatime      0       0" >> /etc/fstab
echo "/dev/voidvm/home  /home       ext4    defaults,noatime      0       0" >> /etc/fstab
echo "/dev/voidvm/swap  swap        swap    defaults              0       0" >> /etc/fstab
echo "$EFI_PARTITION    /boot/efi   vfat    defaults              0       0" >> /etc/fstab

LVM_UUID="blkid -o value -s UUID $LVM_PARTITION"
echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
sed 's/GRUB_CMDLINE_LINUX_DEFAULT="/GRUB_CMDLINE_LINUX_DEFAULT="rd.lvm.vg=voidvm rd.luks.uuid=$LVM_UUID /g' /etc/default/grub

dd bs=1 count=64 if=/dev/urandom of=/boot/volume.key
cryptsetup luksAddKey $LVM_PARTITION /boot/volume.key
chmod 000 /boot/volume.key
chmod -R g-rwx,o-rwx /boot

echo "voidvm   $LVM_PARTITION   /boot/volume.key   luks" >> /etc/crypttab
echo "install_items+=\" /boot/volume.key /etc/crypttab \"" >> /etc/dracut.conf.d/10-crypt.conf
grub-install $EFI_PARTITION
xbps-reconfigure -fa
exit
umount -R /mnt

echo "*** COMPLETED! ***"
